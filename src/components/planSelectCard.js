import React from "react";
import PropTypes from "prop-types";
import Li from "./planSelect-li";
import Select from 'react-select'

export default function planCard(props){
    const options = [
        { value: `1 month : ${props.price1m}`, label: `1 month : ${props.price1m}` },
        { value: `12 month : ${props.price12m} `, label: `12 month : ${props.price12m} saving 11%` },
        { value: `24 month : ${props.price24m}`, label: `24 month : ${props.price24m} saving 17%` },
        { value: `36 month : ${props.price36m} `, label: `36 month : ${props.price36m} saving 23%` }
    ]
    return(
        <div className="planBox border-solid border-2 rounded py-6 hover:border-blue-300 ">
            <div className="px-3 content-center">
                <Select className="w-60 mx-2 text-xs border-gray-200 border-2 rounded-md font-semibold outline-none"
                        options={options}
                        isSearchable={false}
                        placeholder={`24 MONTH : ${props.price24m}  SAVING 17%`}/>

                <h2 className="font-bold text-md text-center capitalize my-3">{props.header}</h2>
                <div id="scrolling" className="scrolling border-solid border-b-2 h-64">
                        <Li header={"setup fees"} desc={"free"}/>
                        <Li header={"uptime guarantee"} desc={"99%"}/>
                        <Li header={"website hosting"} desc={props.websites}/>
                        <Li header={"RAM"} desc={props.RAM}/>
                        <Li header={"free domain"} desc={props.freeDomain}/>
                        <Li header={"bandwidth"} desc={"Unlimited"}/>
                        <Li header={"subdomains"} desc={props.subDomains}/>
                        <Li header={"Parked Domains"} desc={props.parkedDomains}/>
                        <Li header={"Simple DNS Zone Editor"} desc={"yes"}/>
                        <Li header={"Advanced DNS Zone Editor"} desc={"yes"}/>
                        <Li header={"Email Accounts"} desc={props.emailAccounts}/>
                        <Li header={"Free SSL"} desc={"yes"}/>
                        <Li header={"1click installer"} desc={"Yes (SoftAculous)"}/>
                        <Li header={"control panel"} desc={"plesk"}/>
                        <Li header={"webmail"} desc={"yes"}/>
                        <Li header={"MX Entry"} desc={"yes"}/>
                        <Li header={"MySQL Databases"} desc={props.DBs}/>
                        <Li header={"MySQL Database Wizard"} desc={"Yes"}/>
                        <Li header={"phpMyAdmin"} desc={"yes"}/>
                        <Li header={"Remote MySQL Control"} desc={"yes"}/>
                        <Li header={"FTP accounts"} desc={props.FTP}/>
                        <Li header={"file manager"} desc={"yes"}/>
                        <Li header={"custom error page"} desc={"yes"}/>
                        <Li header={"SSL/TLS manager"} desc={"yes"}/>
                        <Li header={"hotLink protection"} desc={"yes"}/>
                        <Li header={"leech protected"} desc={"yes"}/>
                        <Li header={"24/7 technical support"} desc={"yes"}/>
                        <Li header={"data center"} desc={"germany"}/>
                    </div>
                <a className="btn mt-6 self-center font-bold text-sm text-black border-2 border-black px-8 py-2 uppercase inline-block rounded-full " href={props.btnLink}>
                    add to cart
                </a>
            </div>
        </div>
    );
}
planCard.propTypes = {
    price1m: PropTypes.number.isRequired, // must be a number and defined
    price12m: PropTypes.number.isRequired, // must be a number and defined
    price24m: PropTypes.number.isRequired, // must be a number and defined
    price36m: PropTypes.number.isRequired, // must be a number and defined
    header: PropTypes.string.isRequired, // must be a string and defined
    btnLink: PropTypes.string.isRequired, // must be a number and defined
    websites:PropTypes.string.isRequired,
    RAM:PropTypes.string.isRequired,
    freeDomain:PropTypes.string.isRequired,
    subDomains:PropTypes.string.isRequired,
    parkedDomains:PropTypes.string.isRequired,
    emailAccounts:PropTypes.string.isRequired,
    DBs:PropTypes.string.isRequired,
    FTP:PropTypes.string.isRequired,

};