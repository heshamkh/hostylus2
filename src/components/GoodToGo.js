import React from "react";

function GoodToGo() {
  return(
    
  <div className=" flex laptop:my-10 bg-white mobile:flex-col ">

      <div className="laptop:w-2/3 ml-5 mt-10 mobile:text-center tablet:text-center mobile:w-full tablet:w-full">
        <img className="mobile:mt-8  mobile:p-4 m-auto" src="https://ik.imagekit.io/softylus/goodToGo_1__dSM7nZKDo.svg"></img>
      </div>

    <div className="w-1/3 m-auto mobile:text-center tablet:px-4 mobile:m-4 mobile:w-full tablet:w-full ">
           
        <h1 className=" font-extrabold  text-3xl mb-3 ">GOOD TO GO IN SECONDS</h1>
            <p className="font-light  w-3/5 my-3 mobile:w-full mobile:pl-4 mobile:pr-8  tablet:w-full mobile:text-center">
               Get started without waiting! With our friendly interface, you can create server instances almost instantly, usually in under 10 seconds.
               that prevent any imposter from sneak peekingat your data
            </p>
             <div className=" mx-auto flex flex-row">
                   <span className="text-blue-600 font-black uppercase text-md underline mr-2">Learn more</span>
                   <img className="inline-block" src="https://ik.imagekit.io/softylus/arrow_b_EEPW_gX.svg"></img>
               </div>
            </div>
      
  </div>

  
  );
}

export default GoodToGo;
