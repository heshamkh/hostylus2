import React from "react";
import PropTypes from "prop-types";
function wordpressHosting(props) {
    return (
        <div className="h-1/2 bg-gray-100">

            <div className="grid grid-cols-2 p-32 mobile:p-4 justify-items-center mobile:grid-cols-1 tablet:grid-cols-1">
                <div className="laptop:hidden wide-desktop:hidden desktop:hidden" >
                    <img src={props.image}/>
                </div>
                <div className="py-40 mobile:py-8 tablet:py-16 mobile:grid tablet:grid mobile:justify-items-center tablet:justify-items-center">
                    <span className="text-sm mobile:text-xs uppercase font-bold tracking-widest text-black opacity-50 mobile:text-center tablet:text-center">{props.smallHeader}</span>
                    <h1 className="text-4xl  capitalize text-black font-bold my-2 mobile:text-center tablet:text-center">{props.header}</h1>

                    <p className="text-2xl text-gray-700  mt-6 mb-16 mobile:text-center tablet:text-center">{props.desc} <span className="font-black">{props.boldText}</span></p>

                    <a href={props.btnURL} className="bg-blue-600 uppercase text-white font-normal px-8 py-3 rounded-full ">get started</a>
                </div>

                <div className="mobile:hidden tablet:hidden" >
                    <img src={props.image}/>
                </div>
            </div>
        </div>
    )
}
export default wordpressHosting;
wordpressHosting.PropTypes={
    image: PropTypes.string.isRequired, // must be a string and defined
    smallHeader: PropTypes.string.isRequired, // must be a string and defined
    header: PropTypes.string.isRequired, // must be a string and defined
    desc: PropTypes.string.isRequired, // must be a string and defined
    btnURL:PropTypes.string.isRequired,
    boldText:PropTypes.string,
}