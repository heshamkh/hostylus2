import React from "react";
import PropTypes from "prop-types";
export default function planCard(props){
    return(
        <div className="planBox border-solid border-2 rounded py-6 hover:border-blue-300">
            {props.planSVG}
            <span className={`save bg-black text-white px-6 py-2 mb-6 -ml-1 inline-block rounded-r-full uppercase font-bold ${props.hideSave}`}>save {props.savings}%</span>
            <div className="px-6">
            <h2 className="font-bold text-md text-center capitalize mb-3">{props.header}</h2>
            <p className="text-sm text-gray-600 text-center">{props.desc}</p>
            <div className="text-center capitalize"><sup className="text-black text-xl">$</sup><span className="font-bold text-3xl text-blue-700">{props.price}</span><span className="text-gray-500 text-sm">/month</span></div>
            <div className="py-5 border-solid border-b-2">{props.myItems}</div>


            <a className="btn mt-6 self-center font-bold text-sm text-black border-2 border-black px-8 py-2 mx-6 uppercase inline-block rounded-full " href={props.btnLink}>
                get now

            </a></div>
        </div>
    );
}
planCard.propTypes = {
    planSVG: PropTypes.any, // must be a string and defined
    savings: PropTypes.string, // must be a number and defined
    hideSave:PropTypes.string,
    desc: PropTypes.string, // must be a number and defined
    header: PropTypes.string.isRequired, // must be a string and defined
    price: PropTypes.number.isRequired, // must be a number and defined
    myItems:PropTypes.any.isRequired,
    btnLink: PropTypes.string.isRequired, // must be a number and defined
};