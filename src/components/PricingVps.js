import React from "react";
import PropTypes from "prop-types";


export default function vpsPricing(props){
 return(
     <div className="p-4 border-solid border-gray-200 border-2 rounded my-16">
       
         
         <p className="text-black text-3xl text-center font-extrabold my-6">{props.message}</p>
         <div className="w-full h-20 text-center bg-gray-100">
         <span className="text-black ">$</span><span className="m-auto text-blue-600 font-black text-3xl">3.99</span><span className="text-gray-400">/month</span>
         </div>
         <hr className="my-6"/>

         <div className="grid grid-cols-7 gap-2 justify-items-start">
             <img className="rounded-full w-1/3" src={props.image}/>
             <div className="col-span-6">
                 <h3 className="text-black font-bold text-md">{props.name}</h3>
                 <p className="text-gray-400 text-xs">{props.position}</p>
             </div>
         </div>
     </div>
 );
}
vpsPricing.PropTypes={
    image: PropTypes.string.isRequired, // must be a string and defined
    name: PropTypes.string.isRequired, // must be a string and defined
    position: PropTypes.string.isRequired, // must be a number and defined
    message:PropTypes.string.isRequired,
}