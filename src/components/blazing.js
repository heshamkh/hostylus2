import React from "react";

function Blazing() {
  return(
  <div className=" flex m-auto laptop:my-10 bg-white mobile:my-10 mobile:flex-col-reverse ">
    
    <div className="w-1/2  my-auto mobile:my-4 mobile:text-center tablet:align-bottom tablet:my-0 mobile:w-full tablet:w-full ">
           
        <h1 className="  font-extrabold w-2/3 m-auto mobile:mt-5  text-3xl mb-3 ">Blazing Fast Hosting</h1>
            <p className="font-light  w-2/3 my-3 m-auto mobile:w-full mobile:px-10   mobile:text-center">
               We assure you a hosting experience better than anything that you’ve ever had so why compromise for less when you could have more with us?
            </p>
               <div className="w-2/3 mx-auto">
                <button className="  border-black py-2 font-black  rounded-3xl px-8 border-2 my-8 tablet:my-4 mobile:my-4">Get started</button>
               </div>
         
         </div>

        <div className="laptop:w-1/2 mx-10 my-10 mobile:m-auto mobile:text-center tablet:text-center mobile:w-full tablet:w-full">
            <img className="m-auto mobile:p-4" src="https://ik.imagekit.io/softylus/blazing2_xgKjT_QjCw1y.png"></img>
      </div>
  </div>

  );
}

export default Blazing;
