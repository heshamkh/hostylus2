import React from "react";

function Migration() {
  return(
  <div className=" flex laptop:my-10 bg-white mobile:flex-col ">
      <div className="laptop:w-1/2  mt-10 mobile:text-center tablet:text-center mobile:w-full tablet:w-full">
        <img className="mobile:mt-8  mobile:p-4 m-auto" src="https://ik.imagekit.io/softylus/Easy_migration_Awl6S8ifiryCs.png"></img>
      </div>

    <div className="w-1/2 m-auto mobile:text-center tablet:px-4 mobile:m-4 mobile:w-full tablet:w-full ">
           
        <h1 className=" font-extrabold  text-3xl mb-3 ">Easy Migration</h1>
            <p className="font-light  w-3/5 my-3 mobile:w-full mobile:pl-4 mobile:pr-8  tablet:w-full mobile:text-center">
                Fear no loss with Hostylus in data or ranking while migrating your website with recovery plans                     that prevent any imposter from sneak peekingat your data
            </p>
                <button className="border-black py-2 font-black  rounded-3xl px-8 border-2 my-8 mobile:my-4 tablet:my-4">Get started</button>
            </div>
      
  </div>

  
  );
}

export default Migration;
