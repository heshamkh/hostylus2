import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Landing from "../components/landengSection";
// import AmazingPerVPS from "../components/AmazingPerVPS";
// import ExcelentServices from "../components/ExcelentServices";
// import GoodToGo from "../components/GoodToGo";
import FeaturesVPS from "../components/FeaturesVPS";
import InfiniteCarousel from 'react-leaf-carousel';
import PricingVps from "../components/PricingVps"
import ImageLeft from "../components/imageLeft";
import ImageRight from "../components/imageRight";
function vpsHosting() {
    return (
        <Layout>
            <SEO
                keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
                title="Home"
            />
            <Landing smallHeader=""
                     header="HOSTYLUS VPS HOSTING"
                     desc="MORE THAN WHAT YOU HAVE IN MIND, LESS THAN WHAT'S IN ACCOUNT!"
                     boldText="STARTING AT $5.99"
                     btnURL="/"
                     image="https://ik.imagekit.io/softylus/vps_hosting_QZc1NMQX5.svg"

            />
            <ImageRight header={"INCREDIBLE PERFORMANCE STARTING AT $5.99"}
                        desc={"Handle even your most resource-intensive projects with ease. Our cloud servers are located in  state-of-the-art data centers. You get best-in-class performance with AMD EPYC 2nd Gen, Intel® Xeon® Gold processors and speedy NVMe SSDs."}
                        image={"https://ik.imagekit.io/softylus/excelentPEr_zcUvxZZZ7.svg"}
                        hideShopNow={"hidden"}
            />

            <ImageLeft header={"GOOD TO GO IN SECONDS"}
                        desc={"Get started without waiting! With our friendly interface, you can create server instances almost instantly, usually in under 10 seconds that prevent any imposter from sneak peekingat your data"}
                        image={"https://ik.imagekit.io/softylus/goodToGo_1__dSM7nZKDo.svg"}
                        hideShopNow={"hidden"}
            />

            <ImageRight header={"EXCELLENT SERVICES"}
                        desc={"The Hostylus VPS hosting products are provided by provider whom won platinum at the Service Provider Awards 2020. Several thousand readers helped to choose the winner in a survey posted on several IT portals."}
                        image={"https://ik.imagekit.io/softylus/service_G7dzhJ8fW.svg"}
                        hideShopNow={"hidden"}
            />

        <FeaturesVPS />

     <div className="w-4/5 m-auto">
    <InfiniteCarousel
            pauseOnHover={true}
            autoCycle={true}
            breakpoints={[
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    },
                },
            ]}
            dots={true}
            showSides={true}
            sidesOpacity={1}
            sideSize={.1}
            slidesToScroll={1}
            slidesToShow={4}
            scrollOnDevice={true}
        >
            <PricingVps name={"Tom Addam"} position={"Front End Developer"} message={"HX11"} image={"https://ik.imagekit.io/softylus/AdobeStock_287359914_Preview_1_JBCrSxZuB.png"}/>
            <PricingVps name={"Tom Addam"} position={"Front End Developer"} message={"HPX11"} image={"https://ik.imagekit.io/softylus/AdobeStock_287359914_Preview_1_JBCrSxZuB.png"}/>
            <PricingVps name={"Tom Addam"} position={"Front End Developer"} message={"HX21"} image={"https://ik.imagekit.io/softylus/AdobeStock_287359914_Preview_1_JBCrSxZuB.png"}/>
            <PricingVps name={"Tom Addam"} position={"Front End Developer"} message={"HPX21"} image={"https://ik.imagekit.io/softylus/AdobeStock_287359914_Preview_1_JBCrSxZuB.png"}/>
           <PricingVps name={"Tom Addam"} position={"Front End Developer"} message={"HX11"} image={"https://ik.imagekit.io/softylus/AdobeStock_287359914_Preview_1_JBCrSxZuB.png"}/>
            <PricingVps name={"Tom Addam"} position={"Front End Developer"} message={"HPX11"} image={"https://ik.imagekit.io/softylus/AdobeStock_287359914_Preview_1_JBCrSxZuB.png"}/>
            <PricingVps name={"Tom Addam"} position={"Front End Developer"} message={"HX21"} image={"https://ik.imagekit.io/softylus/AdobeStock_287359914_Preview_1_JBCrSxZuB.png"}/>
            <PricingVps name={"Tom Addam"} position={"Front End Developer"} message={"HPX21"} image={"https://ik.imagekit.io/softylus/AdobeStock_287359914_Preview_1_JBCrSxZuB.png"}/>
          
        </InfiniteCarousel>
</div>   
            </Layout> 
       )
    }

    export default vpsHosting;

