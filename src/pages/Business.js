import React from "react";
import SEO from "../components/seo";
import Layout from "../components/layout";
import Feature from "../components/featureBlock";
import Accordion from "../components/accordion";
import ImageRight from "../components/imageRight";
import ImageLeft from "../components/imageLeft";
import Landing from "../components/landengSection";
import LI from "../components/plan-li";
import PlanCard from "../components/planSelectCard";
import Tabs from "../components/tabs";
function business() {
    return(
        <Layout>

            <SEO
                keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
                title="Home"
            />
            <Landing header="Business hosting"
                     desc="Boost your online reach without worrying about your website’s hosting plans. Our wide range of options will help you achieve your intended goals."
                     btnURL="/"
                     image="https://ik.imagekit.io/softylus/worpress_3AA2FdsQ5zgQ.png"

            />
            <section className="plans max-w-5xl mx-auto my-16 mobile:pb-3">
                <h2 className="text-3xl text-black font-bold text-center">Choose Your Best Plan</h2>
                <h6 className="text-gray-400 text-center">High Quality Plans in Fair Rate</h6>
                <div className="grid grid-cols-3 gap-3 flex justify-items-center mt-8 mobile:grid-cols-1 py-8">
                    <PlanCard price1m={11.99} price12m={119.88} price24m={216.00} price36m={288.00}
                              header={"startup"}  btnLink={"/"}
                              websites={"1 website"} RAM={"1000MB"} freeDomain={"no"} subDomains={"1"} parkedDomains={"1"} emailAccounts={"10"} DBs={"6"} FTP={"3"}
                    />
                    <PlanCard price1m={16.99} price12m={179.88} price24m={335.76} price36m={467.64}
                              header={"level up"}  btnLink={"/"}
                              websites={"3 websites"} RAM={"2000MB"} freeDomain={"Yes(.COM)"} subDomains={"4"} parkedDomains={"3"} emailAccounts={"20"} DBs={"3"} FTP={"1"}
                    />
                    <PlanCard price1m={11.99} price12m={119.88} price24m={216.00} price36m={288.00}
                              header={"startup"}  btnLink={"/"}
                              websites={"unlimited"} RAM={"3000MB"} freeDomain={"Yes(.COM)"} subDomains={"unlimited"} parkedDomains={"unlimited"} emailAccounts={"unlimited"} DBs={"unlimited"} FTP={"unlimited"}
                    />
                </div>
            </section>
            <section className="features max-w-6xl mx-auto my-16 mobile:px-3">
                <h2 className="text-4xl text-black font-bold text-center">Business Hosting Features</h2>
                <h4 className="text-gray-400 text-center text-xl px-16 my-3">A domain name tells the world who you are and what you do. Search for a name, word, or phrase and we’ll help you find the perfect domain name to own your piece of the internet.</h4>
                <div className="features grid grid-cols-3 gap-3 flex justify-items-center mt-6 mobile:grid-cols-1 py-8">
                    <Feature image={"https://ik.imagekit.io/softylus/Dns_W_PIp718u.png"}
                             header={"DNS Management"}
                             desc={"Once you use our domain name lookup and find a suitable web address, you can manage your site through a simple interface"}
                    />
                    <Feature image={"https://ik.imagekit.io/softylus/renewal_4usKL-69uosb.png"}
                             header={"Auto-Renewal"}
                             desc={"Protect your domain from expiring and accidentally being lost by enabling auto-renewal. Switch back to manual renewal at anytime. "}
                    />
                    <Feature image={"https://ik.imagekit.io/softylus/Dlock_dn70RqayNC.png"}
                             header={"Domain Lock"}
                             desc={"Once you find your perfect domain, lock it down to prevent unauthorized transfers. Unlock it for transfers at anytime. "}
                    />

                </div>
            </section>
            <ImageRight image={"https://ik.imagekit.io/softylus/server_5gzudTSf0.png"}
                        header={"Migration Hub"}
                        smallHeader={""}
                        desc={"Fear no loss in data or ranking while migrating your website with recovery plans that prevent any imposter from sneak peeking at your data"} hideShopNow={"hidden"}/>
            <ImageLeft image={"https://ik.imagekit.io/softylus/ssl_4R8G7Vw-Nlewi.png"}
                       header={"Website Security Shop SSL Certificates"}
                       desc={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside"}} className="list-disc">
                           <LI LI={"100% SSD cloud hosting"}/>
                           <LI LI={"100% SSD cloud hosting"}/>
                           <LI LI={"100% SSD cloud hosting"}/>
                           <LI LI={"100% SSD cloud hosting"}/>
                           <LI LI={"100% SSD cloud hosting"}/>
                           <LI LI={"100% SSD cloud hosting"}/>
                       </ul>}
                       hideShopNow={"hidden"} hideLearnMore={"hidden"}/>
            <section className="acc py-16 grid justify-items-center">
                <div className=" px-8 max-w-2xl">
                    <h3 className="text-center text-black mb-16 text-4xl font-bold">Entert level Let’s encrypt protection</h3>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>

                </div>
            </section>
            <section className="bg-gray-100 py-16 grid justify-items-center">
                <div className=" px-8 max-w-5xl">
                    <h3 className="text-center text-black mb-4 text-4xl font-bold">Plesk</h3>
                    <h4 className="text-gray-400 text-center mb-8 text-md px-16 my-3">Revealing Plesk next-level server management platform</h4>
                    <Tabs/>
                </div>
            </section>


        </Layout>
    );

}
export default business;